module ProjectRazor
  module ModelTemplate

    class ClusterSLNode < Redhat
      include(ProjectRazor::Logging)

      def initialize(hash)
        super(hash)
        # Static config
        @hidden      = false
        @name        = "cluster_sl_node"
        @description = "Scientific Linux SL 6 Node Model "
        @osversion   = "cluster_sl_node"

        from_hash(hash) unless hash == nil
      end
    end
  end
end
